#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	vector<vector<float>> input, output;
	vector<int> config;
	config = { 2, 10, 10, 1 };
	ANN::LoadData("../learn.txt", input, output);

	auto ann = ANN::CreateNeuralNetwork(config);
	cout << GetTestString().c_str() << endl;
	cout << ann->GetType().c_str() << endl;
	ann->MakeTrain(input, output, 100000, 0.1, 0.1, true);
	ann->Save("../data.txt");
	system("pause");
	return 0;
}