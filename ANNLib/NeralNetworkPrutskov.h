#pragma once
#include "ANN.h"

namespace ANN
{
	class NeralNetworkPrutskov :
		public ANN::NeuralNetwork
	{
	public:
		NeralNetworkPrutskov(std::vector<int> & configuration = std::vector<int>(), NeuralNetwork::ActivationType
			activation_type = NeuralNetwork::POSITIVE_SYGMOID);
		std::string GetType();
		std::vector<float> Predict(std::vector<float> & input);
		float MakeTrain(
			std::vector<std::vector<float>> & inputs,
			std::vector<std::vector<float>> & outputs,
			int max_iters = 10000,
			float eps = 0.1,
			float speed = 0.1,
			bool std_dump = false
			);

		~NeralNetworkPrutskov();
	};



}

