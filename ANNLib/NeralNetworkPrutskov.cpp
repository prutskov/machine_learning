#include "NeralNetworkPrutskov.h"


ANN::NeralNetworkPrutskov::NeralNetworkPrutskov(std::vector<int> & configuration = std::vector<int>(), NeuralNetwork::ActivationType
	activation_type = NeuralNetwork::POSITIVE_SYGMOID)
{
}


ANN::NeralNetworkPrutskov::~NeralNetworkPrutskov()
{
}

std::shared_ptr<ANN::NeuralNetwork> ANN::CreateNeuralNetwork(
	std::vector<int> & configuration,
	NeuralNetwork::ActivationType activation_type)
{
	return std::shared_ptr<ANN::NeuralNetwork>(new ANN::NeralNetworkPrutskov(configuration, activation_type));
}

std::string ANN::NeralNetworkPrutskov::GetType()
{

}

std::vector<float> ANN::NeralNetworkPrutskov::Predict(std::vector<float> & input)
{

}
float ANN::NeralNetworkPrutskov::MakeTrain(
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int max_iters = 10000,
	float eps = 0.1,
	float speed = 0.1,
	bool std_dump = false
	)
{

}